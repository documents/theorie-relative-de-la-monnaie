# Relative Theory of Money (TRM)

Relative Theory of Money (TRM) is a theory about a money system based
on a Universal Dividend calculated from the human life expectancy.
It demonstrate that only one mathematical solution exists to create a "libre money".

You can [read it online here](http://en.trm.creationmonetaire.info/)

You can [download files here](https://gitlab.com/libre-money-projects/theorie-relative-de-la-monnaie/releases)

Text licensed under [GPL version 3](http://www.gnu.org/licenses/gpl.html)

# Théorie Relative de la Monnaie (TRM)

La Théorie Relative de la Monnaie est une théorie décrivant un système monétaire
basé sur un Dividende Universel calculé à partir de l'espérance de vie humaine.
Elle démontre qu'il n'existe qu'une seule solution mathématique pour créer une "monnaie libre".

Vous pouvez [la lire en ligne ici](http://trm.creationmonetaire.info/)

Vous pouvez [télécharger les fichiers ici](https://gitlab.com/libre-money-projects/theorie-relative-de-la-monnaie/releases)

Texte sous licence [GPL version 3](http://www.gnu.org/licenses/gpl.html)


## Setup

This project uses [Sphinx](https://www.sphinx-doc.org/) to build HTML, pdf and ePub versions of the theory.
You will need to install `texlive` and various extensions
and use an adequate Python 3.9+ virtual environment to build it.

- Fedora:

```bash
    $ sudo dnf install texlive texlive-latex latexmk dvipng \
      texlive-fncychap texlive-tabulary texlive-framed texlive-wrapfig \
      texlive-upquote texlive-capt-of texlive-needspace texlive-pgfplots \
      texlive-anyfontsize tex-preview \
      texlive-babel-french texlive-babel-spanish

    $ virtualenv -p `where python3` venv
    $ source venv/bin/activate
    $ pip install -r requirements3.txt
```

- Ubuntu 18.04:

```bash
    $ sudo apt-get install python3-pip python3-dev libfreetype6-dev \
      texlive texlive-latex-extra texlive-latex-recommended \
      texlive-lang-french texlive-lang-spanish \
      dvipng

    $ python3 -m venv .venv
    $ source .venv/bin/activate
    $ pip install -vv -r requirements3.txt
```

## Building

### By language

Build all languages in ``build/fr_FR``, ``build/en_US`` and ``build/es_ES`` folders:

```bash
    $ ./bin/build.sh
```

Build original French version in ``build/fr_FR`` folder:

```bash
    $ ./bin/build_fr_FR.sh
```

Build translated English version in ``build/en_US`` folder:

```bash
    $ ./bin/build_en_US.sh
```

Build translated Spanish version in ``build/es_ES`` folder:

```bash
    $ ./bin/build_es_ES.sh
```

### By format

Generate HTML version:

```bash
    $ make html SPHINXOPTS="-t html"
```

Generate PDF version:

```bash
    $ make latexpdf SPHINXOPTS="-t latex"
```

Generate EPUB version :

```bash
    $ make epub SPHINXOPTS="-t epub"
```

## Translation

### Generating or updating source translation files (.po)
_Do it every time you modified rst files_

```bash
    $ ./bin/update_po_files.sh
```
... Then edit .po files to translate from French...

> Tthere are also few human-readable texts to translate
> in the build scripts (under `build`) and in the themes
> (under `source/themes`).

### Build translation files (.mo).

This will create .mo files:

```bash
    $ sphinx-intl build --locale-dir source/locale
```

When you rebuild in the translated language (here English),
the texts should be properly updated:

```bash
    $ ./bin/build_en_US.sh
```

## Releases

Build Zip archives with version number:

    ./bin/build_release.sh 1.0.0

Commit tag with files updated to new version:

    ./bin/release.sh 1.0.0
