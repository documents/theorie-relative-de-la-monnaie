=============
Formalisation
=============

Principe de relativité
======================

Le principe de relativité tel que défini par Albert Einstein postule qu'
*« aucune propriété des faits
observés ne correspond au concept de repos absolu ; et que dans tous
les systèmes de coordonnées où les équations de la mécanique sont
vraies, les équations électrodynamiques et optiques équivalentes sont
également vraies »* [Albert Einstein (1905), *“De l’électrodynamique des corps en mouvement.”*].
Autrement dit, les lois physiques s'expriment de manière identique (ont la même forme)
dans tous les référentiels, inertiels ou non.
On l'appelle aussi principe de symétrie, ou encore covariance.
Le principe ne signifie pas que les observateurs mesurent la même chose,
mais que les lois physiques que l'on établit doivent,
suite à leur transformation en passant d'un référentiel à l'autre,
avoir la même forme générale. Aussi les mesures sont bien différentes
d'un observateur à l'autre, la seule mesure invariante étant celle de la vitesse de la lumière.

Pour l'économie j'ai étendu ce principe à la notion de monnaie
*« la monnaie, en tant que code universel qui régit les échanges économiques,*
*doit fonctionner de manière identique dans tous les référentiels »*
et de valeur *« tout individu est libre d'estimer ce qui est valeur et ce qui ne l'est pas »*.

Dans l'économie, tout couple observateur / référentiel
est un individu au sein de sa zone monétaire et les principes doivent être valables
et de même forme, quelle que soit la position spatio-temporelle envisagée.
Il s'agit donc aussi d'appliquer le 1er article des droits humains
quant à l'égalité devant le droit entre les individus,
pour l'appliquer non seulement au code qui va régir la monnaie commune,
mais aussi à la mesure relative de toute valeur, qui est aussi la compréhension
de la liberté de choix de l'individu vis-à-vis de la valeur, au niveau de sa production,
autant que de son échange.

Autrement dit *« aucun individu ne doit être privilégié quant au jugement et la mesure de toute valeur »*.

Liberté, valeur, monnaie, repère
================================

On ne peut pas non plus établir un ensemble théorique cohérent
sans préciser les libertés fondamentales auxquels il se réfère.
Ces libertés sont absentes des théories classiques
pour la mauvaise raison qu'elles les ignorent.

\a) Liberté
-----------

La liberté se définit comme étant un principe symétrique :
non-nuisance vis-à-vis de soi-même et d'autrui.


\b) Valeur
----------

On entend par valeur tout bien économique matériel, énergétique,
immatériel, spatial ou temporel. Par exemple on pourra attribuer
une valeur à un fruit, de l'électricité, un logiciel, un terrain
ou un enseignement. Le principe de relativité nie toute mesure absolue de valeur.
Toute valeur est fluctuante relativement à l'individu qui l'utilise,
la produit ou l'échange, elle est donc fluctuante dans tout l'espace-temps considéré.


\c) Monnaie
-----------

La monnaie est un outil de compte et d'échange commun à tous les citoyens
de la même zone économique (par extension on pourra dire « universel »,
en ayant conscience qu'il s'agit d'un « universel » au sein de la zone monétaire considérée).


\d) Zone monétaire
------------------

Une zone monétaire (ou zone économique)
se définit par un espace souverain, et ses citoyens présents et futurs.
Il s'agit donc d'un espace-temps local.


Axiomatique
===========

Le principe de liberté doit s'accorder avec tout individu présent et futur
et nous permet de définir les trois libertés économiques fondamentales
sous la forme des axiomes fondamentaux suivants :


\a) Liberté d'accès aux ressources
----------------------------------

Tout citoyen est libre d'accéder aux ressources.


\b) Liberté de production
-------------------------

Tout citoyen est libre de produire de la valeur.


\c) Liberté d'échange « dans la monnaie »
-----------------------------------------

Tout citoyen est libre d'échanger avec autrui « dans la monnaie ».

La liberté étant définie comme non-nuisance, il ne faut pas tomber
dans l'erreur logique basique qui consisterait à interpréter
les libertés économiques comme un droit de violer la propriété d'autrui,
de produire ou échanger ce qui ne serait pas permis par la Loi.

Comment donc interpréter la « liberté d'accès aux ressources » ?
On doit l'interpréter sous l'angle de la non-nuisance
comme le stipule la « clause lockéenne » :
*« [lorsque quelqu'un s'approprie un objet, il doit en rester]
*suffisamment et en qualité aussi bonne en commun pour les autres »*.
[Clause lockéenne (2018), *Wikipédia*. https://fr.wikipedia.org/wiki/Clause_lock%C3%A9enne]

Par exemple, il n'est pas juste de s'approprier l'unique source d'eau d'un désert,
sans que soit assuré un accès minimal à l'eau pour les autres.


Code libre et système monétaire libre
=====================================

Un code libre tel que défini dans le monde logiciel (« free software »)
consiste en un code de programme informatique ouvert,
et modifiable par ses utilisateurs.
Il s'oppose à un code dit « propriétaire » ou « privateur »,
dont l'accès et le droit de modification reste à la discrétion
d'une license dérivée d'un droit de propriété,
et permet au propriétaire de priver ses utilisateurs
de la liberté d'adapter le code à ses besoins,
et de l'utiliser dans le cadre qu'il souhaite.

Ce principe de « liberté du code »
est fondamentalement compatible avec le principe de relativité,
parce que si les lois sont indépendantes du référentiel,
c'est bien qu'elles ne sont ni cachées, ni inaccessibles
via l'expérimentation où que l'on se trouve.

Or le contrôle de la monnaie est actuellement comparable à un code privateur,
dans le sens où la monnaie est contrôlée par des règles non modifiables démocratiquement.
Par exemple, les règles de Bâle 1, 2 et 3
n'ont pas été établies suivant un processus démocratique
et les opérations d'émission de crédits asymétriques par le système bancaire ne sont pas transparentes.
La crise historique des « subprimes » qui a vu son sommet en 2008
en est la dernière illustration en date.

Selon les conséquences de la « perspective numérique » révélée par Olivier Auber,
le choix d'un système implique le choix du code qui le régit, et n'est pas neutre.
On doit donc se poser la question de la transparence et de la légitimité du code.

Ceci implique que la liberté du code qui régit un système
(ici la monnaie, code de tous les échanges économiques), est une notion préalable au choix,
sinon il n'y a tout simplement pas de choix, et donc pas de liberté.
Selon ce critère défendu par l'inventeur du logiciel libre Richard Stallman,
si vous acceptez d'utiliser un système dont le code n'est pas libre,
vous vous privez de libertés fondamentales.

La conséquence d'un système monétaire à code privateur,
est l'émergence d'une économie dont le champ de valeur
est une structure topologique pyramidale auto-reproductive et instable.
Par contre la conséquence de l'utilisation d'un système monétaire libre
est l'émergence d'une économie dont le champ de valeur est une structure sphérique
en expansion dans l'espace-temps, compatible avec le renouvellement des générations.

On fera toutefois la différence entre les libertés logicielles définies
par la Free Software Foundation (FSF), qui sont au nombre de quatre,
et celles liées aux libertés d'un protocole de communication
ou d'échange comme la monnaie, qui lui ne peut pas être modifié individuellement
sans se couper de la communauté qui l'utilise.
Ainsi pour le logiciel libre, les libertés définies par la FSF sont :

* Liberté d'utilisation
* Liberté d'accès au code source
* Liberté de modification du code source
* Liberté de copie

Qui sont différentes des quatres libertés qui doivent être associées à un
système monétaire libre :

* Liberté de modification démocratique
* Liberté d'accès aux ressources
* Liberté de production de valeurs
* Liberté d'échange « dans la monnaie »

Par exemple en 2021, l'euro ne peut pas être considéré comme une monnaie
d'un système monétaire libre puisque son code (les accords sur le code monétaire)
ne sont pas modifiés via un processus démocratique.

Nous pouvons parler de l'euro comme d'une monnaie privatrice de liberté,
ou encore un système monétaire privateur, *au moins* au sens de la première liberté
et plus encore selon la quatrième liberté comme nous le verrons par la suite.

Autre exemple : l'or. Nous pouvons parler de l'or comme d'un *candidat monétaire*
ne respectant pas *au moins* la troisième liberté économique d'échange *« dans la monnaie »*,
pour la raison simple - que nous développerons par la suite -
qu'il n'est pas universellement accessible au sein d'une zone économique.
Une telle « monnaie » qui force le retour au troc là où elle n'est pas présente,
ne peut avoir la caractéristique de liberté « d'échange dans la monnaie ».

Et c'est pourquoi la TRM fait la différence entre une valeur spécifique
et la monnaie *« mesure et moyen d'échange universel »* au sein de la zone monétaire.

C'est un peu le même rôle que joue la vitesse de la lumière au sein de la physique relativiste.
La lumière n'est pas un objet physique comme les autres. Sa vitesse, donnée d'espace/temps
(une distance divisée par un temps) est la même dans tous les référentiels.
Et c'est parce que les observateurs s'accordent sur ce point,
qu'ils en déduisent la relativité des autres mesures pour établir
une théorie relativiste compatible entre eux, donnant des mesures différentes
selon les référentiels, mais « de même forme ».

Résumé
======

Nous voici donc munis des fondements suivants :

* Principe de relativité
* Liberté de modification démocratique
* Liberté d'accès aux ressources
* Liberté de productions
* Liberté d'échange « dans la monnaie »
