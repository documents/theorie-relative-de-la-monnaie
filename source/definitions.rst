===========
Définitions
===========

On ne peut valablement étudier l'économie sans faire appel à un repère
et une mesure de référence des échanges, de la même façon qu'en toute science,
le repère considéré et les unités de mesure se doivent d'être définis avant toute étude.

Tout comme un repère et des unités de temps et de longueur sont nécessaires
à l'établissement des lois physiques, aucune étude poussée ne peut être menée
sans que ne soient définis auparavant le repère de référence de l'économie
et l'unité de mesure associée.

Repère : la zone monétaire
==========================

Une zone économique ou zone monétaire constitue le repère de base
de l'étude économique. Qu'est-ce qui la caractérise ?

* L'espace où l'accord monétaire est manifeste

* Le temps, c'est-à-dire, l'espérance de vie moyenne des individus qui y vivent et y meurent.

* La production individuelle ou collective (entrepreneuriale) de biens et de services

* L'échange de biens et de services entre individus ou groupements d'individus.

Les individus ou groupements d'individus sont inévitablement amenés à échanger,
ne serait-ce que de l'information, de l'éducation, ou plus généralement encore du lien.
Ce qui caractérise donc fondamentalement la zone économique
c'est l'ensemble des individus qui la composent.
L'économie existe partout et en tout temps dès que des individus produisent
et échangent des biens et des services, et cela quelle que soit la nature de ces biens et services.
Par contre, on ne peut pas définir de zone économique vide d'individus.
C'est donc bien l'individu qui constitue la seule valeur commune
et fondamentale de tout repère économique valide.

Mais pour aller plus loin, cet ensemble d'individus évolue dans le temps
avec les naissances et les morts, l'immigration et l'émigration.
La zone économique peut donc se représenter comme un espace temps discret
en constante création/destruction où chaque point temporaire
représente un individu à durée de vie limitée.

Il s'agit donc d'une trame spatio-temporelle en transformation continue,
non statique, discrète, où chaque point d'espace-temps est créé à une date déterminée
(naissance d'un individu) et a une durée de vie limitée qui, en moyenne,
correspond à l'espérance de vie que l'on nommera :math:`ev` de la zone économique considérée.

Par ailleurs et c'est la définition fondamentale de la Relativité en économie,
tout individu a une vision personnelle et unique de la valeur de toute chose
et aucun des individus ou groupement d'individus d'une zone économique donnée
n'est en mesure d'imposer aux autres une vision particulière
de ce qui est de la valeur ou ce qui n'en est pas.


Zone économique pseudo-isolée
=============================

Une zone économique est dite pseudo-isolée quand, pour une durée de temps donnée,
on peut considérer qu'elle vit de façon autonome ou quasi-autonome vis-à-vis de l'extérieur.
Ce peut-être le cas d'économies sur des îles encore autonomes,
où la subsistance des individus est assurée par une production alimentaire suffisante
(ce qui est là aussi tout à fait relatif,  on peut par exemple étudier le cas de certains ascètes),
mais aussi le cas d'un groupement d'individus topologiquement complexe,
répartis dans un espace non connexe, transnational, voire transcontinental.
Du moment que cet ensemble manifeste une autonomie, on peut le considérer
comme une zone économique pseudo-isolée, capable d'auto-gérer son flux de production et d'échanges,
au moins sur une petite période de temps.


Mesure de valeur : Les échanges monétisés
=========================================

Quand il y a échange de biens et/ou de services, on parle d'échanges de valeurs.
Quand Alice et Bob échange des biens ou des services respectifs :math:`x` et :math:`y`,
ils échangent des valeurs respectives :math:`V_x` et :math:`V_y`.
Cet échange est équitable si :math:`V_x = V_y`.
En termes monétaires, ces valeurs peuvent s'écrire :math:`V_x = P_x.C_x`,
où :math:`P_x` est le prix unitaire de :math:`x` et :math:`C_x` sa quantité.

Cette définition de la valeur reste relative à l'observateur qui la mesure.
Ainsi, même si Alice considère que :math:`V_x = V_y`,
Bob pourrait considérer que :math:`V_x < V_y` (et ne s'en porte pas moins bien).
Par ailleurs Charles, qui observerait cet échange,
pourrait considérer depuis son propre référentiel que ces deux valeurs sont nulles.

Le désaccord sur la valeur des biens est historique, voire humain.
Ainsi l'égalité des valeurs échangées n'est pas un critère économique indépendant de
l'observateur, ce qui se constate aussi par les actions de dons ou de taxations
sans retours, et donc non symétriques, où l'égalité des valeurs échangées n'est
pas respectée en fonction du point de vue.

C'est pour des besoins d'universalité de la mesure de valeur,
que les individus s'accordent sur une mesure commune d'échange qu'ils appellent monnaie.
Une monnaie définie donne ainsi une mesure commune de valeur à toute chose dans la même unité,
pour un repère d'observation donné, ce qui permet des comparaisons plus aisées.

La monnaie joue donc le rôle non seulement d'outil d'échange entre individus de la zone économique,
mais aussi de seule valeur indépendante du référentiel d'observation.

Ceci étant posé, il existe historiquement et localement,
un grand nombre de définitions différentes de la monnaie,
qui impliquent des types d'échanges fondamentalement différents,
et qui sont le plus souvent ignorées par ceux qui acceptent de l'utiliser.

Ces cas d'ignorance de la nature de la monnaie utilisée le plus souvent de façon contrainte,
constituent une violation du droit contractuel de base en économie,
qui suppose l'acceptation des parties impliquées quant au type d'échange proposé.

On peut sans hésiter déclarer que l'imposition d'usage d'une monnaie non
contractuelle (ne faisant pas l'objet d'une acceptation volontaire) constitue un
acte contraire aux droits humains de disposer de sa vie pour sa part
économique et une violation des principes constitutionnels de liberté et
d'égalité.

Et donc *a minima* dans une démocratie, la monnaie officielle ne peut être acceptable
que si elle fait l'objet d'une élaboration démocratique dans sa définition,
ainsi que dans sa validation, son acceptation, sa modification et son abandon.
