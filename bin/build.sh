#!/usr/bin/env bash
set -e

# build French version
./bin/build_fr_FR.sh

# build English version
./bin/build_en_US.sh

# build Spanish version
./bin/build_es_ES.sh
